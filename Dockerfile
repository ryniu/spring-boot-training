FROM openjdk:8-jdk-alpine as build
WORKDIR /app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN apk update && apk add dos2unix && dos2unix mvnw
RUN chmod +x ./mvnw
RUN ./mvnw dependency:go-offline -B



RUN ./mvnw package -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)


FROM openjdk:8-jdk-alpine

ENV DB_PASSWORD=${DB_PASSWORD:-REPLACE}
ENV DB_USER=${DB_USER:-REPLACE}
ENV DB_HOST=${DB_HOST:-REPLACE}
ENV DB_PORT=${DB_PORT:-REPLACE}
ENV DB_NAME=${DB_NAME:-REPLACE}

ARG DEPENDENCY=/app/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app


ENTRYPOINT ["java","-cp","app:app/lib/*","-Dspring.profiles.active=prod","pl.sii.test.TestApplication"]