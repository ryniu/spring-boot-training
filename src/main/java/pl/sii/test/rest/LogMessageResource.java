package pl.sii.test.rest;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sii.test.domain.MessageCategory;
import pl.sii.test.repository.MessageCategoryRepository;
import pl.sii.test.rest.request.MessageCategoryRequest;
import pl.sii.test.service.dto.MessageCategoryDTO;
import pl.sii.test.service.mapper.MessageCategoryMapper;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/logmessage")
public class LogMessageResource {
    private final Logger log = LoggerFactory.getLogger(LogMessageResource.class);

    private MessageCategoryRepository messageCategoryRepository;
    @Autowired
    private MessageCategoryMapper messageCategoryMapper;

    public LogMessageResource(MessageCategoryRepository messageCategoryRepository) {
        this.messageCategoryRepository = messageCategoryRepository;
    }

    @RequestMapping(value = "/get/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<MessageCategoryDTO>> getOperationsWithoutFilter(@PathVariable String name) {
        List<MessageCategory> messageCategory = messageCategoryRepository.findByName(name);
        return ResponseEntity.ok().header("Access-Control-Allow-Origin", "*").body(messageCategory.stream().map(e -> messageCategoryMapper.transform(e)).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/all", method = RequestMethod.POST)
    public ResponseEntity<Page<MessageCategoryDTO>> getOperations(@ApiParam @RequestBody MessageCategoryRequest messageCategoryRequest, @ApiParam Pageable pageable) {
        Page<MessageCategory> messageCategorys =  messageCategoryRepository.findByName(messageCategoryRequest.getName(),pageable);
        return ResponseEntity.ok().header("Access-Control-Allow-Origin", "*").body(messageCategorys.map(messageCategoryMapper::transform));
    }
}