package pl.sii.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.sii.test.domain.MessageCategory;
import pl.sii.test.domain.MessageType;
import pl.sii.test.repository.MessageCategoryRepository;
import pl.sii.test.repository.MessageTypeRepository;

import java.util.Optional;

@SpringBootApplication
public class TestApplication {
    private static final Logger log = LoggerFactory.getLogger(TestApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(MessageCategoryRepository repository, MessageTypeRepository messageTypeRepository) {
        return (args) -> {

            MessageCategory messageCategory1 = MessageCategory.builder().name("test1").build();
            MessageCategory messageCategory2 = MessageCategory.builder().name("test2").build();
            MessageCategory messageCategory3 = MessageCategory.builder().name("test3").build();
            MessageCategory messageCategory4 = MessageCategory.builder().name("test4").build();

            MessageType mtype1 = MessageType.builder().messageCategory(messageCategory1).name("cat1").priority(1).build();
            MessageType mtype2 = MessageType.builder().messageCategory(messageCategory2).name("cat2").priority(1).build();
            MessageType mtype3 = MessageType.builder().messageCategory(messageCategory3).name("cat3").priority(1).build();


            messageTypeRepository.save(mtype1);
            messageTypeRepository.save(mtype2);
            messageTypeRepository.save(mtype3);


            log.info("MessageCategory found with findAll():");
            log.info("-------------------------------");
            for (MessageCategory customer : repository.findAll()) {
                log.info(customer.getName());
            }
            log.info("");

            Optional<MessageCategory> byId = repository.findById(1L);
            log.info("MessageCategory found with findById(1L):");
            log.info("--------------------------------");
            log.info(byId.get().getName());
            log.info("");

            log.info("MessageCategory found with findByName('test1'):");
            log.info("--------------------------------------------");
            repository.findByName("test1").forEach(test -> {
                log.info(test.getName());
            });
            log.info("");
        };
    }


}
