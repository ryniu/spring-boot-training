package pl.sii.test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.sii.test.domain.MessageCategory;

import java.util.List;

public interface MessageCategoryRepository extends JpaRepository<MessageCategory, Long>, PagingAndSortingRepository<MessageCategory, Long> {
    List<MessageCategory> findByName(String name);

    Page<MessageCategory> findByName(String name, Pageable pageable);
}