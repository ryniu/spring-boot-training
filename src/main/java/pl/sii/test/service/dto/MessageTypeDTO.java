package pl.sii.test.service.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import pl.sii.test.domain.MessageCategory;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;


@Setter(value = AccessLevel.PUBLIC)
@Getter
public class MessageTypeDTO {
    private String name;
    private int priority;
    private MessageCategoryDTO messageCategoryDTO;
}
