package pl.sii.test.service.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;


@Setter(value = AccessLevel.PUBLIC)
@Getter
public class MessageCategoryDTO {
    private String name;
}
