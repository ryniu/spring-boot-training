package pl.sii.test.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sii.test.domain.MessageCategory;
import pl.sii.test.domain.MessageType;
import pl.sii.test.service.dto.MessageCategoryDTO;
import pl.sii.test.service.dto.MessageTypeDTO;

@Service
public class MessageTypeMapper {

    @Autowired
    private MessageCategoryMapper messageCategoryMapper;

    public MessageTypeDTO transform(MessageType messageType) {
        if (messageType == null)
            return null;
        MessageTypeDTO messageTypeDTO = transformMessageType(messageType);
        messageTypeDTO.setMessageCategoryDTO(messageCategoryMapper.transform(messageType.getMessageCategory()));
        return messageTypeDTO;
    }

    public MessageTypeDTO transformMessageType(MessageType messageType) {
        if (messageType == null)
            return null;
        MessageTypeDTO messageTypeDTO = new MessageTypeDTO();
        messageTypeDTO.setName(messageType.getName());
        messageTypeDTO.setPriority(messageType.getPriority());
        return messageTypeDTO;
    }
}
