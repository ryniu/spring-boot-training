package pl.sii.test.service.mapper;

import org.springframework.stereotype.Service;
import pl.sii.test.domain.LogMessage;
import pl.sii.test.domain.MessageCategory;
import pl.sii.test.service.dto.MessageCategoryDTO;

import java.util.stream.Collectors;

@Service
public class MessageCategoryMapper {
    public MessageCategoryDTO transform(MessageCategory messageCategory) {
        if (messageCategory == null)
            return null;
        MessageCategoryDTO messageCategoryDTO = new MessageCategoryDTO();
        messageCategoryDTO.setName(messageCategory.getName());
        return messageCategoryDTO;
    }
}
