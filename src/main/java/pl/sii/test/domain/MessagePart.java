package pl.sii.test.domain;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@Entity
@Table(name = "messagepart")
public class MessagePart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Length(max = 255)
    @Column(name = "name", updatable = false, nullable = false)
    private String name;

    @NotNull
    @OneToMany(mappedBy = "messagePart", orphanRemoval = true)
    private List<LogMessage> logMessages;

    @ManyToMany(mappedBy = "messageParts")
    private List<Source> sources;

}
