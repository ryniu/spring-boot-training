package pl.sii.test.domain;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@Table(name = "logmessage")
public class LogMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Length(max = 255)
    @Column(name = "time", updatable = false, nullable = false)
    private Date time;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private MessageType messageType;

    @Column(name = "body", updatable = false, nullable = true)
    private String body;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "messagepart_id")
    private MessagePart messagePart;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Date logTime;
}
