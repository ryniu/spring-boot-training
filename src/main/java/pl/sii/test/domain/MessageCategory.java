package pl.sii.test.domain;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;


@Entity
@Table(name = "messagecategory")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
public class MessageCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Length(max = 255)
    @Column(name = "name", updatable = false, nullable = false)
    private String name;
}
